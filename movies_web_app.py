import os
import time
import json
import decimal

from flask import request
from flask import Flask, render_template
import mysql.connector
from mysql.connector import errorcode

application = Flask(__name__)
app = application


# Helper class to convert a DynamoDB item to JSON. 
# Copied from https://docs.aws.amazon.com/amazondynamodb/latest/developerguide/GettingStarted.Python.04.html#GettingStarted.Python.04.Scan
class DecimalEncoder(json.JSONEncoder):
    def default(self, o):
        if isinstance(o, decimal.Decimal):
            return str(o)
        return super(DecimalEncoder, self).default(o)


# Helper method to return connection to mysql server
def get_cnx(username, password, hostname, db):
    cnx = ''

    try:
        cnx = mysql.connector.connect(user=username, password=password,
                                      host=hostname,
                                      database=db)
    except Exception as exp:
        print(exp)
        import MySQLdb
        cnx = MySQLdb.connect(unix_socket=hostname, user=username, passwd=password, db=db)

    return cnx


def get_db_creds():
    db = os.environ.get("DB", None) or os.environ.get("database", None)
    username = os.environ.get("USER", None) or os.environ.get("username", None)
    password = os.environ.get("PASSWORD", None) or os.environ.get("password", None)
    hostname = os.environ.get("HOST", None) or os.environ.get("dbhost", None)

    return db, username, password, hostname


def create_table():
    # Check if table exists or not. Create and populate it only if it does not exist.
    # releaseDate can be TEXT since no validation required (piazza post 213)
    db, username, password, hostname = get_db_creds()
    table_ddl = 'CREATE TABLE movies(id INT UNSIGNED NOT NULL AUTO_INCREMENT, '\
                                     'year INT UNSIGNED,'\
                                     'title VARCHAR(255) NOT NULL UNIQUE, '\
                                     'director TEXT, '\
                                     'actor TEXT, '\
                                     'releaseDate TEXT, '\
                                     'rating FLOAT, '\
                                     'PRIMARY KEY (id))'

    cnx = get_cnx(username, password, hostname, db)
    cur = cnx.cursor()

    try:
        cur.execute(table_ddl)
        cnx.commit()
    except mysql.connector.Error as err:
        if err.errno == errorcode.ER_TABLE_EXISTS_ERROR:
            print("already exists.")
        else:
            print(err.msg)


def query_data():

    db, username, password, hostname = get_db_creds()

    print("Inside query_data")
    print("DB: %s" % db)
    print("Username: %s" % username)
    print("Password: %s" % password)
    print("Hostname: %s" % hostname)

    cnx = get_cnx(username, password, hostname, db)

    cur = cnx.cursor()

    cur.execute("SELECT * FROM movies")
    # entries = [dict(title=row[0]) for row in cur.fetchall()]
    #return entries
    return cur.fetchall()

try:
    print("---------" + time.strftime('%a %H:%M:%S'))
    print("Before create_table")
    create_table()
    print("After create_table")
except Exception as exp:
    print("Got exception %s" % exp)
    conn = None


@app.route('/add_movie', methods=['POST'])
def add_movie():
    print("Received request.")
    print(request.form)

    year = request.form['year']
    title = request.form['title']
    director = request.form['director']
    actor = request.form['actor']
    releaseDate = request.form['release_date']
    rating = request.form['rating']

    # Only title is required
    mutation_columns = 'title'
    mutation_data = "'" + title + "'"
    
    if year:
        mutation_columns = mutation_columns + ', year'
        mutation_data = mutation_data + ', ' + year
    if director:
        mutation_columns = mutation_columns + ', director'
        mutation_data = mutation_data + ", '" + director + "'"
    if actor:
        mutation_columns = mutation_columns + ', actor'
        mutation_data = mutation_data + ", '" + actor + "'"
    if releaseDate:
        mutation_columns = mutation_columns + ', releaseDate'
        mutation_data = mutation_data + ", '" + releaseDate + "'"
    if rating:
        mutation_columns = mutation_columns + ', rating'
        mutation_data = mutation_data + ', ' + rating

    db, username, password, hostname = get_db_creds()

    cnx = get_cnx(username, password, hostname, db)

    try:
        cur = cnx.cursor()
        cur.execute("INSERT INTO movies (" + mutation_columns + ") values (" + mutation_data + ")")
        cnx.commit()
    except Exception as exp:
        print(exp)
        message = "Movie " + title + " could not be inserted - " + str(exp)
        return render_template('index.html', message=message)

    message = "Movie " + title + " successfully inserted"
    return render_template('index.html', message=message)


def query_movie_by_title(cur, title):
    try:
        cur.execute("SELECT title FROM movies WHERE title='" + title + "'")
    except Exception as exp:
        raise exp
    
    return cur.fetchall()


@app.route('/update_movie', methods=['POST'])
def update_movie():
    print("Received request.")
    print(request.form)

    year = request.form['year']
    title = request.form['title']
    director = "'" + request.form['director'] + "'"
    actor = "'" + request.form['actor'] + "'"
    releaseDate = "'" + request.form['release_date'] + "'"
    rating = request.form['rating']

    if not year:
        year = "''"
    if not rating:
        rating = "''"

    db, username, password, hostname = get_db_creds()

    cnx = get_cnx(username, password, hostname, db)

    try:
        cur = cnx.cursor()
        if len(query_movie_by_title(cur, title)) > 0:
            cur.execute("UPDATE movies SET year=" + year + ", director=" + director + '' \
                        ", actor=" + actor + ", releaseDate=" + releaseDate + '' \
                        ", rating=" + rating + " WHERE title='" + title + "'")
            cnx.commit()
        else:
            message = "Movie with " + title + " does not exist"
            return render_template('index.html', message=message)
    except Exception as exp:
        print(exp)
        message = "Movie " + title + " could not be updated - " + str(exp)
        return render_template('index.html', message=message)

    message = "Movie " + title + " successfully updated"
    return render_template('index.html', message=message)


@app.route("/delete_movie", methods=['POST'])
def delete_movie():
    print("Received request.")
    print(request.form['delete_title'])

    title = request.form['delete_title']

    db, username, password, hostname = get_db_creds()

    cnx = get_cnx(username, password, hostname, db)

    try:
        cur = cnx.cursor()
        if len(query_movie_by_title(cur, title)) > 0:            
            cur.execute("DELETE FROM movies WHERE title='" + title + "'")
            cnx.commit()
        else:
            message = "Movie with " + title + " does not exist"
            return render_template('index.html', message=message)
    except Exception as exp:
        print(exp)
        message = "Movie " + title + " could not be deleted - " + str(exp)
        return render_template('index.html', message=message)

    message = "Movie " + title + " successfully deleted"
    return render_template('index.html', message=message)


@app.route("/search_movie", methods=['GET'])
def search_movie():
    print("Received request.")
    print(request.args.get('search_actor'))

    actor = request.args.get('search_actor')

    db, username, password, hostname = get_db_creds()

    cnx = get_cnx(username, password, hostname, db)

    message = ''

    try:
        cur = cnx.cursor()
        cur.execute("SELECT title, year, actor FROM movies WHERE actor='" + actor + "'")
        movies_with_given_actor = cur.fetchall()
        if len(movies_with_given_actor) > 0:
            resulting_string = ''
            for movie in movies_with_given_actor:
                resulting_string = resulting_string + json.dumps(movie, cls=DecimalEncoder)

                # Add newline character if not the last movie in list
                if movie != movies_with_given_actor[len(movies_with_given_actor) - 1]:
                    resulting_string = resulting_string + ' -- '

            message = resulting_string

        else:
            message = "No movies found for " + actor
    except Exception as exp:
        print(exp)
        message = str(exp)

    return render_template('index.html', message=message)


@app.route("/highest_rating", methods=['GET'])
def highest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = get_cnx(username, password, hostname, db)

    message = ''

    try:
        cur = cnx.cursor()
        cur.execute("SELECT title, year, actor, director, rating FROM movies WHERE rating="\
                    "(SELECT MAX(rating) FROM movies)")
        movies_with_highest_rating = cur.fetchall()
        if len(movies_with_highest_rating) > 0:
            resulting_string = ''
            for movie in movies_with_highest_rating:
                resulting_string = resulting_string + json.dumps(movie, cls=DecimalEncoder)

                # Add newline character if not the last movie in list
                if movie != movies_with_highest_rating[len(movies_with_highest_rating) - 1]:
                    resulting_string = resulting_string + ' -- '

            message = resulting_string

        else:
            message = "No movies found with highest rating"
    except Exception as exp:
        print(exp)
        message = str(exp)

    return render_template('index.html', message=message)


@app.route("/lowest_rating", methods=['GET'])
def lowest_rating():
    print("Received request.")

    db, username, password, hostname = get_db_creds()

    cnx = get_cnx(username, password, hostname, db)

    message = ''

    try:
        cur = cnx.cursor()
        cur.execute("SELECT title, year, actor, director, rating FROM movies WHERE rating="\
                    "(SELECT MIN(rating) FROM movies)")
        movies_with_highest_rating = cur.fetchall()
        if len(movies_with_highest_rating) > 0:
            resulting_string = ''
            for movie in movies_with_highest_rating:
                resulting_string = resulting_string + json.dumps(movie, cls=DecimalEncoder)

                # Add newline character if not the last movie in list
                if movie != movies_with_highest_rating[len(movies_with_highest_rating) - 1]:
                    resulting_string = resulting_string + ' -- '

            message = resulting_string

        else:
            message = "No movies found with lowest rating"
    except Exception as exp:
        print(exp)
        message = str(exp)

    return render_template('index.html', message=message)


@app.route("/")
def hello():
    print("Printing available environment variables")
    print(os.environ)
    print("Before displaying index.html")
    # movies = query_data()
    # print("Entries: %s" % movies)
    message = "Welcome to my CD-enabled Jenkins app!!!"
    return render_template('index.html', message=message)


if __name__ == "__main__":
    app.debug = True
    app.run(host='0.0.0.0')
